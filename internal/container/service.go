package container

import (
	"context"
	"io"

	"github.com/docker/docker/client"
)

type Event uint8

const (
	EventCompleted Event = iota
	EventError
	EventTimeout
)

type Service interface {
	Run(context.Context, io.Reader, io.Writer) (chan Event, chan error)
}

type DockerClient struct {
	client  *client.Client
	workers map[string]bool
}

func NewClient(cli *client.Client) DockerClient {
	return DockerClient{
		client:  cli,
		workers: make(map[string]bool),
	}
}
