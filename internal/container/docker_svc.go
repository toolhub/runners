package container

import (
	"context"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

// TODO: maybe include the context here after making sure it's a sane idea via google.com ofc.
type work struct {
	in  io.Reader
	out io.Writer
}

type dockerContainerWorker struct {
	Client        *client.Client
	ContainerID   string
	PunchQueue    chan *dockerContainerWorker
	WorkChannel   chan work
	ResultChannel chan Event
	ErrorChannel  chan error
}

func (d dockerContainerWorker) Run(w work) error {
	err := d.Client.ContainerStart(context.Background(), d.ContainerID, types.ContainerStartOptions{})
	if err != nil {
		return fmt.Errorf("failed to start container %s: %s", d.ContainerID, err)
	}

	waiter, err := d.Client.ContainerAttach(context.Background(), d.ContainerID, types.ContainerAttachOptions{
		Stream: true,
		Stdin:  true,
		Stdout: true,
		Stderr: true,
	})
	if err != nil {
		return fmt.Errorf("failed to attach to container %s: %s", d.ContainerID, err)
	}

	defer waiter.Close()

	go func() {
		io.Copy(waiter.Conn, w.in)
		waiter.CloseWrite()
	}()
	go io.Copy(w.out, waiter.Reader)

	statusChan, errChan := d.Client.ContainerWait(context.Background(), d.ContainerID, container.WaitConditionNotRunning)
	select {
	case s := <-statusChan:
		if s.Error != nil {
			fmt.Printf("wait result; error from container %s: %s\n", d.ContainerID, err)
		}
		return nil
	case e := <-errChan:
		return e
	}
}

func (d dockerContainerWorker) Start() {
	go func() {
		for {
			// Notify readiness.
			d.PunchQueue <- &d

			// Get work.
			if err := d.Run(<-d.WorkChannel); err != nil {
				d.ErrorChannel <- err
				continue
			}

			// Report completion.
			d.ResultChannel <- EventCompleted
		}
	}()
}

func newWorker(cli *client.Client, id string, punchQueue chan *dockerContainerWorker) dockerContainerWorker {
	return dockerContainerWorker{
		Client:        cli,
		ContainerID:   id,
		PunchQueue:    punchQueue,
		WorkChannel:   make(chan work),
		ResultChannel: make(chan Event),
		ErrorChannel:  make(chan error),
	}
}

type DockerContainerService struct {
	client     *client.Client
	workers    []dockerContainerWorker
	punchQueue chan *dockerContainerWorker
}

func NewDockerContainerService(cli *client.Client, task Task, workerCount uint) (*DockerContainerService, error) {
	svc := &DockerContainerService{
		client:     cli,
		punchQueue: make(chan *dockerContainerWorker),
	}
	if err := svc.prepare(context.Background(), task, workerCount); err != nil {
		return nil, err
	}
	return svc, nil
}

func (d *DockerContainerService) ensureImage(ctx context.Context, task Task) error {
	images, err := d.client.ImageList(ctx, types.ImageListOptions{All: true})
	if err != nil {
		return fmt.Errorf("failed to get image list: %s", err)
	}

	if !strings.Contains(task.Image, ":") {
		task.Image = fmt.Sprintf("%s:latest", task.Image)
	}

	for _, image := range images {
		for _, tag := range image.RepoTags {
			if tag == task.Image { // found the image.
				return nil
			}
		}
	}

	reader, err := d.client.ImagePull(ctx, task.Image, types.ImagePullOptions{})
	if reader != nil {
		defer reader.Close()
	}
	if err != nil {
		return fmt.Errorf("failed to pull image %s: %s", task.Image, err)
	}
	io.Copy(os.Stdout, reader)

	return nil
}

func (d *DockerContainerService) prepare(ctx context.Context, task Task, workerCount uint) error {
	if err := d.ensureImage(ctx, task); err != nil {
		return err
	}

	// check existing containers.
	containers, err := d.client.ContainerList(ctx, types.ContainerListOptions{All: true})
	if err != nil {
		return fmt.Errorf("could not get container list: %s", err)
	}

	availableContainers := map[string]string{}

	nameMatcher := regexp.MustCompile(fmt.Sprintf(`cfi-%s-\d$`, task.Name))
	for _, container := range containers {
		for _, name := range container.Names {
			if !nameMatcher.MatchString(name) {
				continue
			}
			var duration time.Duration = 0 * time.Second
			err := d.client.ContainerStop(ctx, container.ID, &duration)
			if err != nil {
				return fmt.Errorf("failed to stop container %s: %s", name, err)
			}
			containerName := strings.TrimLeft(name, "/")
			availableContainers[containerName] = container.ID
			break
		}
	}

	for i := uint(0); i < workerCount; i++ {
		name := fmt.Sprintf("cfi-%s-%d", task.Name, i)
		if _, ok := availableContainers[name]; ok {
			continue
		}
		container, err := d.client.ContainerCreate(ctx, &container.Config{
			Image:     task.Image,
			OpenStdin: true,
			StdinOnce: true,
		}, nil, nil, nil, name)
		if err != nil {
			return fmt.Errorf("failed to create container %s: %s", name, err)
		}
		availableContainers[name] = container.ID
	}

	// make the container workers and assign them a container id.
	for _, id := range availableContainers {
		worker := newWorker(d.client, id, d.punchQueue)
		worker.Start()
		d.workers = append(d.workers, worker)
		fmt.Printf("worker %s is ready\n", worker.ContainerID)
	}

	return nil
}

func (d *DockerContainerService) Run(ctx context.Context, in io.Reader, out io.Writer) (chan Event, chan error) {
	// wait for a worker to sign up
	worker := <-d.punchQueue

	// submit the work.
	worker.WorkChannel <- work{in, out}

	// return worker's result and error channel and return.
	return worker.ResultChannel, worker.ErrorChannel
}
