package container

type Task struct {
	Name     string
	Image    string
	Reusable bool
}
