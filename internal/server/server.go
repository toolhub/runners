package server

import (
	"net/http"

	"gitlab.com/toolhub/runners/internal/container"
)

type DockerCallService struct {
	containerService container.Service
}

func NewDockerCallService(containerService container.Service) *DockerCallService {
	return &DockerCallService{
		containerService: containerService,
	}
}

func (s *DockerCallService) ServeHTTP(response http.ResponseWriter, request *http.Request) {
	statusChan, errChan := s.containerService.Run(request.Context(), request.Body, response)
	if request.Body != nil {
		defer request.Body.Close()
	}
	select {
	case result := <-statusChan:
		switch result {
		case container.EventTimeout:
			response.WriteHeader(http.StatusRequestTimeout)
		}
	case err := <-errChan:
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(err.Error()))
	}
}
