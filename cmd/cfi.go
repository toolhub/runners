package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/docker/docker/client"
	"gitlab.com/toolhub/runners/internal/container"
	"gitlab.com/toolhub/runners/internal/server"
)

func makeService(workerCount uint, name, image string, reusable bool) (*server.DockerCallService, error) {
	opts := []client.Opt{}
	dockerHost := os.Getenv("DOCKER_HOST")
	if len(dockerHost) != 0 {
		opts = append(opts, client.WithHost(dockerHost))
	}
	cli, err := client.NewClientWithOpts(opts...)
	if err != nil {
		return nil, fmt.Errorf("could not create docker client: %s", err)
	}
	task := container.Task{
		Name:     name,
		Image:    image,
		Reusable: reusable,
	}
	containerSvc, err := container.NewDockerContainerService(cli, task, workerCount)
	if err != nil {
		return nil, fmt.Errorf("could not create docker container service: %s", err)
	}
	return server.NewDockerCallService(containerSvc), nil
}

func main() {
	workerCount := flag.Uint("w", 1, "number of container workers.")
	name := flag.String("name", "worker", "name of the task. used mostly to create containers. default: worker")
	image := flag.String("image", "", "image for the task.")
	reusable := flag.Bool("stream", false, "sets whether or not the CFI supports streams.")
	address := flag.String("address", "0.0.0.0:4000", "address to listen on.")
	flag.Parse()

	svc, err := makeService(*workerCount, *name, *image, *reusable)
	if err != nil {
		log.Fatalf("could not start docker call service: %s", err)
	}

	log.Fatalln(http.ListenAndServe(*address, svc))
}
