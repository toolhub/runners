IMAGE := toolhub/cfi
REGISTRY := registry.hub.docker.com
TARGET := builder
TAG := latest

IMAGE_TAG_FULL = $(REGISTRY)/$(IMAGE):$(TAG)


server:
	go run cmd/http.go --image cfi-sample -w 2 --address 0.0.0.0:5000

client:
	go run cmd/client/http.go

build:
	docker build -f docker/Dockerfile --target $(TARGET) -t $(IMAGE_TAG_FULL) .

run:
	docker run --rm $(IMAGE_TAG_FULL)

push:
	docker push $(IMAGE_TAG_FULL)
